const tape = require('tape')
const TestBot = require('../test-bot')
const { isMsgId } = require('ssb-ref')
const { SaveCollection } = require('../lib/helpers')

const MB = 1024 * 1024

const image = {
  blob: '&CLbw5B9d5+H59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI=.sha256',
  unbox: '4bOkwCLbw5B9d5+H59oxDNOyIaOhfLfqOLm1MGKyTLI=.boxs',
  mimeType: 'image/png',
  size: 4 * MB,
  width: 500,
  height: 480
}

tape('saveCollection (create)', async (t) => {
  t.plan(3)
  const { ssb, apollo } = await TestBot({ loadContext: true })

  const save = SaveCollection(apollo, t)

  const whoami = await apollo.query({
    query: `{
      whoami { 
        public {
          feedId
          profileId
        }
        personal {
          groupId
          profileId
        }
      } 
    } `
  })

  t.error(whoami.errors, 'whoami returns no errors')

  const { personal } = whoami.data.whoami

  const input = {
    name: 'Collection',
    description: 'this is a collection',
    image,
    authors: {
      add: ['*']
    }
  }

  const id = await save({ type: 'test', recps: [personal.groupId], ...input })
  t.true(isMsgId(id), 'save returns messageId')

  ssb.close()
})

tape('saveCollection (update)', async (t) => {
  t.plan(7)
  const { ssb, apollo } = await TestBot()
  const save = SaveCollection(apollo, t)

  const collectionId = await save({
    type: 'test',

    name: 'Some name',
    description: 'this is a collection',
    image,
    authors: {
      add: [ssb.id]
    },
    recps: [ssb.id]
  })

  t.true(
    isMsgId(collectionId),
    'saves a fully featured collection, returning collectionId'
  )

  const initialGet = await apollo.query({
    query: `query($input: ID!) {
      collection(id: $input) {
        type
        name
        description
        image {
          blob
          mimeType
          size
          width
          height
        }
        submissionDate
        recps
      }
    }`,
    variables: {
      input: collectionId
    }
  })

  delete image.unbox

  t.error(initialGet.errors, 'gets profile without error')

  t.deepEqual(
    initialGet.data.collection,
    {
      type: 'test',
      name: 'Some name',
      description: 'this is a collection',
      image,
      submissionDate: null,
      recps: [ssb.id]
    },
    'can retrieve collection'
  )

  /* update */
  const update = await save({
    id: collectionId, // << this makes it an update

    name: 'Some New Name'
  })

  t.error(update.errors, 'saves update with no errors')

  const updatedGet = await apollo.query({
    query: `query($input: ID!) {
      collection(id: $input) {
        id
        type
        name
      }
    }`,
    variables: {
      input: collectionId
    }
  })

  t.deepEqual(
    updatedGet.data.collection,
    {
      id: collectionId,
      type: 'test',
      name: 'Some New Name'
    },
    'can update collection'
  )

  ssb.close()
})
