function SaveCollection (apollo, t) {
  return async function saveCollection (input) {
    const res = await apollo.mutate({
      mutation: `mutation($input: CollectionInput) {
        saveCollection(input: $input)
      }`,
      variables: { input }
    })

    t.error(res.errors, 'save collection without errors')

    return res.data.saveCollection
  }
}

function GetCollections (apollo, t) {
  return async function getCollections (filter = {}, expectedError) {
    const res = await apollo.query({
      query: `query($filter: StoryFilter) {
        collections(filter: $filter) {
          id
          name
          recps
          type
        }
      }`,
      variables: {
        filter
      }
    })

    if (!expectedError) t.error(res.errors, 'gets collections without errors')
    else t.match(res.errors, expectedError)

    return res.data.collections
  }
}

function GetCollection (apollo, t) {
  return async function getCollection (id) {
    const res = await apollo.query({
      query: `query($id: ID!) {
        collection(id: $id) {
          id
          name
          recps
          type
          storyLinks {
            linkId
            recps
            story {
              id
              title
            }
          }
        }
      }`,
      variables: { id }
    })

    t.error(res.errors, 'gets collection without errors')

    return res.data.collection
  }
}

function SaveStory (apollo, t) {
  return async function save (input) {
    const res = await apollo.mutate({
      mutation: `mutation($input: StoryInput) {
        saveStory(input: $input)
      }`,
      variables: { input }
    })

    t.error(res.errors, 'saves story without error')

    return res.data.saveStory // returns msgId
  }
}

function GetStory (apollo, t) {
  return async function get (id) {
    const res = await apollo.query({
      query: `query($id: ID!) {
        story(id: $id) {
          id
          recps
          collections: collectionLinks {
            linkId
            collection {
              id
              name
            }
          }
        }
      }`,
      variables: { id }
    })

    t.error(res.errors, 'gets story without error')

    return res.data.story
  }
}

function SaveCollectionStoryLink (apollo, t) {
  return async function saveCollectionStoryLink (input) {
    const res = await apollo.mutate({
      mutation: `mutation($input: CollectionStoryLinkInput) {
        saveCollectionStoryLink(input: $input)
      }`,
      variables: {
        input
      }
    })

    t.error(res.errors, 'saves story link without errors')

    return res.data.saveCollectionStoryLink
  }
}

module.exports = {
  SaveCollection,
  GetCollections,
  SaveStory,
  SaveCollectionStoryLink,
  GetCollection,
  GetStory
}
