const tape = require('tape')
const TestBot = require('../test-bot')
const { GetCollections, SaveCollection } = require('../lib/helpers')

tape('getCollections', async (t) => {
  t.plan(7)
  const { ssb, apollo } = await TestBot()

  const saveCollection = SaveCollection(apollo, t)
  const getCollections = GetCollections(apollo, t)

  const recps = [ssb.id]

  const input = (type, name) => {
    return {
      type,
      name,
      authors: {
        add: [ssb.id]
      }
    }
  }

  // save a bunch of collections
  const collection1 = await saveCollection({ ...input('test', 'Day 1'), recps })
  const collection2 = await saveCollection({ ...input('test', 'Day 2'), recps })
  const collection3 = await saveCollection(input('test-2', 'Day 3')) // save this one without recps and a different type

  // fetch all collections by groupId
  const actualCollectionsByRecps = await getCollections({ groupId: ssb.id, type: 'test' })

  const expectedCollectionsByRecps = [
    {
      id: collection1,
      type: 'test',
      name: 'Day 1',
      recps
    },
    {
      id: collection2,
      type: 'test',
      name: 'Day 2',
      recps
    }
  ]

  t.deepEqual(
    actualCollectionsByRecps,
    expectedCollectionsByRecps,
    'returns expected collections by recps'
  )

  // gets stories by type
  const actualCollectionsByType = await getCollections({ type: 'test-2' })

  const expectedCollectionsByType = [
    {
      id: collection3,
      type: 'test-2',
      name: 'Day 3',
      recps: null
    }
  ]

  t.deepEqual(
    actualCollectionsByType,
    expectedCollectionsByType,
    'returns expected stories by type'
  )

  // const allCollections = await getCollections({  })

  // const expectedAllCollections = [
  //   ...expectedCollectionsByRecps,
  //   ...expectedCollectionsByType
  // ]

  // t.deepEqual(
  //   allCollections,
  //   expectedAllCollections
  // )

  ssb.close()
})
