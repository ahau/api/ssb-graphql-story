const tape = require('tape')
const TestBot = require('../test-bot')

function SaveStory (apollo, t) {
  return async function saveStory (input) {
    const res = await apollo.mutate({
      mutation: `mutation($input: StoryInput) {
        saveStory(input: $input)
      }`,
      variables: { input }
    })

    t.error(res.errors, 'save story without errors')

    return res.data.saveStory
  }
}

function GetStories (apollo, t) {
  return async function getStories (filter = {}, expectedError) {
    const res = await apollo.query({
      query: `query($filter: StoryFilter) {
        stories(filter: $filter) {
          id
          title
          recps
          type
        }
      }`,
      variables: {
        filter
      }
    })

    if (!expectedError) t.error(res.errors, 'gets stories without errors')
    else t.match(res.errors, expectedError)

    return res.data.stories
  }
}

tape('getStories', async (t) => {
  t.plan(7)
  const { ssb, apollo } = await TestBot()

  const saveStory = SaveStory(apollo, t)
  const getStories = GetStories(apollo, t)

  const recps = [ssb.id]

  const input = (type, title) => {
    return {
      type,
      title,
      authors: {
        add: [ssb.id]
      }
    }
  }

  // save a bunch of stories
  const story1 = await saveStory({ ...input('test', 'Day 1'), recps })
  const story2 = await saveStory({ ...input('test', 'Day 2'), recps })
  const story3 = await saveStory(input('test-2', 'Day 3')) // save this one without recps and a different type

  // fetch all stories by groupId
  const actualStoriesByRecps = await getStories({ groupId: ssb.id, type: 'test' })

  const expectedStoriesByRecps = [
    {
      id: story1,
      type: 'test',
      title: 'Day 1',
      recps
    },
    {
      id: story2,
      type: 'test',
      title: 'Day 2',
      recps
    }
  ]

  t.deepEqual(
    actualStoriesByRecps,
    expectedStoriesByRecps,
    'returns expected stories by recps'
  )

  // gets stories by type
  const actualStoriesByType = await getStories({ type: 'test-2' })

  const expectedStoriesByType = [
    {
      id: story3,
      type: 'test-2',
      title: 'Day 3',
      recps: null
    }
  ]

  t.deepEqual(
    actualStoriesByType,
    expectedStoriesByType,
    'returns expected stories by type'
  )

  // const allStories = await getStories()

  // const expectedAllStories = [
  //   ...expectedStoriesByRecps,
  //   ...expectedStoriesByType
  // ]

  // t.deepEqual(
  //   allStories,
  //   expectedAllStories
  // )

  ssb.close()
})
