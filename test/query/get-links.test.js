const tape = require('tape')
const TestBot = require('../test-bot')
const { SaveCollection, SaveStory, SaveCollectionStoryLink, GetCollection, GetStory } = require('../lib/helpers')

tape('links (get)', async (t) => {
  t.plan(19)

  const { ssb, apollo } = await TestBot({ recpsGuard: true })

  const saveStory = SaveStory(apollo, t)
  const saveCollection = SaveCollection(apollo, t)
  const saveCollectionStoryLink = SaveCollectionStoryLink(apollo, t)
  const getCollection = GetCollection(apollo, t)
  const getStory = GetStory(apollo, t)

  const authors = {
    add: ['*']
  }

  // save a collection
  const collectionId = await saveCollection({
    type: 'test',
    name: 'My Holiday',
    authors,
    recps: [ssb.id]
  })

  const collection2Id = await saveCollection({
    type: 'test',
    name: 'Christmas',
    authors,
    recps: [ssb.id]
  })

  // save some stories to link to the collection
  const story1Id = await saveStory({
    type: 'test',
    title: 'Day 1',
    description: 'Travelled to the camp',
    authors,
    recps: [ssb.id]
  })

  const story2Id = await saveStory({
    type: 'test',
    title: 'Day 2',
    description: 'Swam in the ocean',
    authors,
    recps: [ssb.id]
  })

  const story3Id = await saveStory({
    type: 'test',
    title: 'Day 3',
    description: 'Went fishing',
    authors,
    recps: [ssb.id]
  })

  // save the links between the collection and stories
  const story1LinkId = await saveCollectionStoryLink({
    type: 'collection-story',
    collection: collectionId,
    story: story1Id,
    authors,
    recps: [ssb.id]
  })

  const story2LinkId1 = await saveCollectionStoryLink({
    type: 'collection-story',
    collection: collectionId,
    story: story2Id,
    authors,
    recps: [ssb.id]
  })

  // story 2 will be linked to two collections
  const story2LinkId2 = await saveCollectionStoryLink({
    type: 'collection-story',
    collection: collection2Id,
    story: story2Id,
    authors,
    recps: [ssb.id]
  })

  // story 3 will be linked to holiday collection
  const story3LinkId = await saveCollectionStoryLink({
    type: 'collection-story',
    collection: collectionId,
    story: story3Id,
    authors,
    recps: [ssb.id]
  })

  // get the storyLinks
  const collection = await getCollection(collectionId)

  t.deepEqual(
    collection,
    {
      id: collectionId,
      name: 'My Holiday',
      recps: [ssb.id],
      type: 'test',
      storyLinks: [
        {
          linkId: story1LinkId,
          story: {
            id: story1Id,
            title: 'Day 1'
          },
          recps: [ssb.id]
        },
        {
          linkId: story2LinkId1,
          story: {
            id: story2Id,
            title: 'Day 2'
          },
          recps: [ssb.id]
        },
        {
          linkId: story3LinkId,
          story: {
            id: story3Id,
            title: 'Day 3'
          },
          recps: [ssb.id]
        }
      ]
    }
  )

  // update the collection
  await saveCollection({
    id: collectionId,
    name: '2020 Holiday'
  })

  // tombstone the first story
  await saveStory({
    id: story1Id,
    tombstone: {
      date: Date.now()
    }
  })

  // update the second story
  await saveStory({
    id: story2Id,
    title: 'Day Two - Swimming'
  })

  // tombstone the link between collection and the third story
  await saveCollectionStoryLink({
    linkId: story3LinkId,
    tombstone: {
      date: new Date()
    }
  })

  // get the updated stories through the collection
  const updatedCollection = await getCollection(collectionId)
  t.deepEqual(
    updatedCollection,
    {
      id: collectionId,
      name: '2020 Holiday',
      recps: [ssb.id],
      type: 'test',
      storyLinks: [
        {
          linkId: story2LinkId1,
          story: {
            id: story2Id,
            title: 'Day Two - Swimming'
          },
          recps: [ssb.id]
        }
      ]
    }
  )

  // get the second story and linked collections
  const story = await getStory(story2Id)

  t.deepEqual(
    story,
    {
      id: story2Id,
      recps: [ssb.id],
      collections: [
        {
          linkId: story2LinkId1,
          collection: {
            id: collectionId,
            name: '2020 Holiday'
          }
        },
        {
          linkId: story2LinkId2,
          collection: {
            id: collection2Id,
            name: 'Christmas'
          }
        }
      ]
    }
  )

  ssb.close()
})
