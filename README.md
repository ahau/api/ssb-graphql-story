# ssb-graphql-story

GraphQL types and resolvers for the ssb-story plugin

## Dependencies

`ssb-query`

## Usage

Install ssb-graphqls main:

`npm i -S @ssb-graphql/main`

## Example Usage

```javascript
const ahauServer = require('ahau-graphql-server')

const Server = require('ssb-server')
const Config = require('ssb-config/inject')

const config = Config({})

const sbot = Server
  .use(require('ssb-backlinks'))
  .use(require('ssb-story'))
  .call(null, config)

const main = require('@ssb-graphql/main')(sbot)
const story = require('@ssb-graphql/story')(sbot)

main.Context(sbot, (err, context) => {
  if (err) throw err

  ahauServer({
    schemas: [
      main,
      story
    ],
    context,
    port
  }, (err, httpServer) => {
    //ready!
  })
})
```
