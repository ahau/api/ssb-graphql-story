const blobToURI = require('ssb-serve-blobs/id-to-url')
const ssbResolvers = require('./ssb')
const { STORY, COLLECTION } = require('../src/lib/constants')

module.exports = function Resolvers (sbot, externalGetters) {
  const {
    getStory,
    getStories,
    postSaveStory,

    getCollection,
    getCollections,
    postSaveCollection,

    postSaveCollectionStoryLink,

    // links
    getLinkedStoriesByCollection,
    getLinkedCollectionsByStory,

    gettersWithCache
  } = ssbResolvers(sbot, externalGetters)

  function bulkGet (tangle, getter) {
    /*
      type: the subType of a collection, story, link etc. E.g. iwi for story/iwi
      groupId: the feedId of a group/tribe
      collection: to sort by a
    */
    return async function (_, { filter: { type: subType, groupId } }) {
      const filter = {}

      if (groupId) filter.recps = [groupId]
      if (subType) filter.type = `${tangle}/${subType}`

      return getter(filter)
    }
  }

  const resolvers = {
    Story: {
      collectionLinks: ({ id }) => getLinkedCollectionsByStory(id)
    },
    Collection: {
      storyLinks: ({ id }) => getLinkedStoriesByCollection(id)
    },
    Query: {
      story: (_, { id }) => getStory(id),
      stories: bulkGet(STORY, getStories),
      collection: (_, { id }) => getCollection(id),
      collections: bulkGet(COLLECTION, getCollections)
    },
    CollectionImage: {
      uri (image) {
        const unbox = (image.uri) ? image.unbox || new URL(image.uri).searchParams.get('unbox') : image.unbox

        // TODO pivot on type - could be hyperBlobs?
        const port = sbot.config.serveBlobs && sbot.config.serveBlobs.port
        return blobToURI(image.blob, { unbox, port })
      }
    },
    Mutation: {
      saveStory: (_, { input }) => postSaveStory(input),
      saveCollection: (_, { input }) => postSaveCollection(input),
      saveCollectionStoryLink: (_, { input }) => {
        // save() expects id not linkId
        if (input.linkId) {
          input.id = input.linkId
          delete input.linkId
        }

        // renaming hacks
        if (input.collection) {
          input.parent = input.collection
          delete input.collection
        }

        if (input.story) {
          input.child = input.story
          delete input.story
        }

        return postSaveCollectionStoryLink(input)
      }
    }
  }

  return {
    resolvers,
    gettersWithCache
  }
}
