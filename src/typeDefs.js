const { gql } = require('graphql-tag')

module.exports = gql`
  """
    StoryInput which handles the properties allowed on input for creating and updating an artefact
  """
  input StoryInput {
    id: ID,
    type: String
    title: String,
    description: String,
    timeInterval: EdtfInterval, # The start and/or end date(s) of the story
    submissionDate: EdtfDate
    location: String
    contributionNotes: String
    locationDescription: String
    format: String
    identifier: String
    language: String
    source: String
    transcription: String
    permission: String
    
    tombstone: TombstoneInput
    recps: [String]
    authors: StoryAuthorSetInput
  }

  input StoryAuthorSetInput {
    add: [String]
    remove: [String]
  }

  type Story {
    id: ID,
    type: String
    title: String,
    description: String,
    timeInterval: EdtfInterval, # the start and/or end date(s) of the record
    submissionDate: EdtfDate
    location: String
    contributionNotes: String
    locationDescription: String
    format: String
    identifier: String
    language: String
    source: String
    transcription: String
    permission: String

    tombstone: Tombstone
    recps: [String]

    collectionLinks: [LinkStoryToCollection]
  }

  input CollectionInput {
    id: ID
    type: String
    name: String
    description: String
    image: CollectionImageInput
    submissionDate: EdtfDate
    recordCount: Int

    tombstone: TombstoneInput
    recps: [String]
    authors: StoryAuthorSetInput
  }

  """
  Input for creating a ssb-story image
  """
  input CollectionImageInput {
    blob: ID
    unbox: String
    mimeType: String
    size: Int
    width: Int
    height: Int
    uri: String
  }


  type Collection {
    id: ID
    type: String
    name: String
    description: String
    image: CollectionImage
    submissionDate: EdtfDate
    recordCount: Int

    tombstone: Tombstone
    recps: [String]

    storyLinks: [LinkCollectionToStory]
  }

  input CollectionStoryLinkInput {
    linkId: ID
    type: String

    collection: ID
    story: ID

    tombstone: TombstoneInput
    recps: [String]
    authors: StoryAuthorSetInput
  }

  """
    WARNING: used underscore as StoryLink is already defined in
    ssb-graphql-whakapapa, but because this module doesnt know about that one,
    (and we want to keep it that way) it uses its own
  """
  type LinkCollectionToStory {
    linkId: ID
    story: Story

    tombstone: Tombstone
    recps: [String]
  }

  type LinkStoryToCollection {
    linkId: ID
    collection: Collection
    
    tombstone: Tombstone
    recps: [String]
  }


  """
  Image fields for the ssb-story plugin
  """
  type CollectionImage {
    blob: ID
    mimeType: String
    size: Int
    width: Int
    height: Int
    uri: String
  }


  input StoryFilter {
    groupId: String
    type: String! # this is the sub type 
  }

  extend type Query {
    story(id: ID!): Story
    stories(filter: StoryFilter): [Story]

    collection(id: ID!): Collection
    collections(filter: StoryFilter): [Collection]
  }

  extend type Mutation {
    saveStory(input: StoryInput): ID
    saveCollection(input: CollectionInput): ID
    saveCollectionStoryLink(input: CollectionStoryLinkInput): ID
  }
`
