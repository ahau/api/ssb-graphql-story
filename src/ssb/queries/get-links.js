// TODO: can probably reuse BulkGet but use this for now...

const pull = require('pull-stream')
const paraMap = require('pull-paramap')

module.exports = function GetLinks (sbot, getter, tangle) {
  return function getLinks (input = {}, cb) {
    sbot.story.collection.getStoryLinks(input, (err, links) => {
      if (err) return cb(err)

      pull(
        pull.values(prune(links)),
        paraMap((link, cb) => {
          link.collection = link.parent
          link.story = link.child

          // use the getter to fetch the full object of the type (e.g. story)
          getter(link[tangle], (err, linkItem) => {
            if (err) {
              console.error(`Unable to get story ${link[tangle]}, error:`)
              console.error(err)
              return cb(null, null)
              // HACK : don't kill all record getting if just one fails
            }

            cb(null, {
              ...link,
              linkId: link.key,
              key: undefined,
              [tangle]: linkItem
            })
          })
        }, 4),
        pull.filter(Boolean), // filter out null values
        pull.filter(link => (link.tombstone === null && link[tangle].tombstone === null)), // filter out tombstoned items
        pull.collect((err, links) => {
          if (err) return cb(err)

          cb(null, links)
        })
      )
    })
  }
}

// Compares each object and deduplicates any items that have the same ID.
function prune (nodes) {
  const seen = new Set()

  return nodes.filter(({ key }) => {
    if (seen.has(key)) {
      return false
    } else {
      seen.add(key)
      return true
    }
  })
}
