const pull = require('pull-stream')
const pullParamap = require('pull-paramap')
const { where, sortByArrival, slowEqual, toPullStream, and, type } = require('ssb-db2/operators')

// TODO: move this into ssb-story, rewrite so it skips the getter step
module.exports = function BulkGet (sbot, tangle, getter) {
  return function bulkGet (filter = {}, cb) {
    if (!filter.type) throw new Error('filter.type is required for fast index')

    pull(
      sbot.db.query(
        where(and(
          type(filter.type),
          slowEqual(`value.content.tangles.${tangle}.root`, null)
        )),
        sortByArrival(),
        toPullStream()
      ),
      pull.filter(m => filter.recps ? m.value.content?.recps?.[0] === filter.recps[0] : true),
      pull.map(root => root.key),
      pullParamap(getter, 6),
      pull.filter(item => item.tombstone === null),
      pull.filter(Boolean), // drop items which have some trouble resolving
      pull.collect((err, data) => {
        if (err) return cb(err)
        cb(null, data)
      })

    )
  }
}
