const buildTransformation = require('../../lib/build-transformation')

module.exports = function Save ({ create, update }, tangle) {
  return function save (input, cb) {
    const { id, type } = input

    const T = buildTransformation(input)

    if (id) {
      update(id, T, (err, _) => {
        if (err) cb(err)
        else cb(null, id)
      })
      return
    }

    if (!type) return cb(new Error(`Type is required for creating a new ${tangle}`))

    create(type, T, (err, id) => {
      if (err) cb(err)
      else cb(null, id)
    })
  }
}
